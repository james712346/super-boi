import discord
import asyncio
import MySQLdb
import os
import time
import datetime
Electives={'ipt':'IPT','mu':'Music','its':'ITS', 'ts':'Tech Studies','chem':'Chemistry', 'phy':'Physics','bio':'Biology', 's21': 'Science 21', 'gra':'Graphics', 'mt':'Music Tech','his':'History','tafe':'Tafe'}
All_subjects={'ipt':'IPT','mu':'Music','its':'ITS', 'ts':'Tech Studies','chem':'Chemistry', 'phy':'Physics','bio':'Biology', 's21': 'Science 21', 'gra':'Graphics', 'mt':'Music Tech','his':'History','tafe':'Tafe', "Maths A":"Maths A", "Maths B":"Maths B", "Maths C":"Maths C","English":"English"}
Full_name_all_subjects=[]
for i in All_subjects:
    Full_name_all_subjects.append(All_subjects[i])
class Users:
    def __init__(self, user, sex, hobbies, s1, s2, s3, s4, m1, m2, eng):
        self.user = user
        self.sex = sex
        self.hobbies = hobbies
        self.sub1 = s1
        self.sub2 = s2
        self.sub3 = s3
        self.sub4 = s4
        self.maths1 = m1
        self.maths2 = m2
        self.eng = eng
    def readable(self, v):
        if v == 'M':
            if self.maths2 == '1':
                v+='aths A'
            if self.maths1 == '1':
                if not v == 'M':
                    v+= ", M"
                v+='aths B'
            if self.maths2 == '2':
                if not v == 'M':
                    v+= ", M"
                v+='aths C'
            return v
        if v == 'E':
            english_directory={'0':'English Communication','1':'Normal English','2':'Adv English'}
            return english_directory[self.eng]
    async def idtouser(self, mesg):
        return discord.utils.get(mesg.server.members, id=self.user)
    async def roles(self, usermethord):
        Roles=''
        for i in usermethord.roles:
            Roles+= str(i) if len(usermethord.roles)-1 == usermethord.roles.index(i) else str(i)+", " if not (i == '@everyone' or i in Full_name_all_subjects) else ""
        return Roles
    async def roleslist(self, usermethord):
        Roles=[]
        for i in usermethord.roles:
            Roles.append(i)
        return Roles
    async def roleadd(self, mesg):
        subrole=[discord.utils.get(mesg.server.roles, name=self.sub1),discord.utils.get(mesg.server.roles, name=self.sub2),discord.utils.get(mesg.server.roles, name=self.sub3)]
        if self.maths1 == '1':
            subrole.append(discord.utils.get(mesg.server.roles, name="Maths B"))
        if self.maths2 == '1':
            subrole.append(discord.utils.get(mesg.server.roles, name="Maths A"))
        if self.maths2 == '2':
            subrole.append(discord.utils.get(mesg.server.roles, name="Maths C"))
        if self.eng == '1':
            subrole.append(discord.utils.get(mesg.server.roles, name="English"))
        if self.sub4 != 'null':
            subrole.append(discord.utils.get(mesg.server.roles, name=self.sub4))
        name=discord.utils.get(mesg.server.members, id=self.user)
        for i in All_subjects:
            Role=discord.utils.get(mesg.server.roles, name=All_subjects[i])
            while Role in name.roles:
                await client.remove_roles(name, Role)
        for i in subrole:
            while not i in name.roles:
                await client.add_roles(name, i)

async def addinguser(type, user, sex, hobbies, sub1, sub2, sub3, sub4, m1, m2, eng):
    conn2 = MySQLdb.connect (host = "localhost",user = "root",passwd = "password",db = "discord")
    cursor2 = conn2.cursor(MySQLdb.cursors.DictCursor)
    if type == '0':
        sql = "'"+user+"','"+sex+"','"+hobbies+"','"+m1+"','"+m2+"','"+eng+"','"+sub1+"','"+sub2+"','"+sub3+"','"+sub4+"'"
        print(sql)
        cursor2.execute("INSERT INTO discord (users, sex, hobbies, maths1, maths2, english, subject1, subject2, subject3, subject4) VALUES ("+sql+")")
        print('added')
    else:
        sql = "`users`="+"'"+user+"'"+",`sex`="+"'"+sex+"'"+",`hobbies`="+"'"+hobbies+"'"+",`subject1`="+"'"+sub1+"'"+",`subject2`="+"'"+sub2+"'"+",`subject3`="+"'"+sub3+"'"+",`subject4`="+"'"+sub4+"'"+",`maths1`="+"'"+m1+"'"+",`maths2`="+"'"+m2+"'"+",`english`="+"'"+eng+"'"+"where users = '"+user+"'"
        sql = "UPDATE `discord` SET "+sql
        print(sql)
        cursor2.execute(sql)
        print('updated')
    conn2.commit()
    time.sleep(10)
    conn2.close()
    users[user]=Users(user,sex,hobbies,sub1,sub2,sub3,sub4,m1,m2,eng)

conn = MySQLdb.connect (host = "localhost",user = "root",passwd = "password",db = "discord")
cursor = conn.cursor(MySQLdb.cursors.DictCursor)
cursor.execute('SELECT * from discord')
users={}
for i in cursor.fetchall():
    users[i['users']]=Users(i['users'],i['sex'],i['hobbies'],i['subject1'],i['subject2'],i['subject3'],i['subject4'],i['maths1'],i['maths2'],i['english'])

client = discord.Client()
@client.event
async def on_ready():
    print('Logged in as')
    print(client.user.name)
    print(client.user.id)
    print("loaded",len(users),"people")
    print('------')
@client.event
async def on_message(message):
    global users
    if message.author == client.user:
        return
    if message.content.lower().startswith('!setup'):
        member_role = discord.utils.get(message.server.roles, name='Members')
        print(message.author.roles)
        if discord.utils.get(message.server.roles, name='Staff') in message.author.roles:
            await client.send_message(message.channel, 'Do you want to setup the bot or your username? ("Bot" or "User")')
            rep = await client.wait_for_message(author=message.author)
            if str(rep.content).lower() == 'bot':
                await client.send_message(message.channel, 'Setting up bot...')
                await client.send_message(message.channel, 'Add any subjects? ("if no type null")')
                rep = await client.wait_for_message(author=message.author)
                while rep.content.lower() != 'null':
                    await client.send_message(message.channel, 'Key for '+str(rep.content)+' eg. chem for Chemistry')
                    rep1 = await client.wait_for_message(author=message.author)
                    await client.send_message(message.channel, 'Add any subjects? ("if no type null")')
                    rep = await client.wait_for_message(author=message.author)
                for i in All_subjects:
                    if not discord.utils.get(message.server.roles, name=All_subjects[i]) in message.server.roles:
                        await client.create_role(message.server, name=All_subjects[i])
                        time.sleep(5)
                    else:
                        pass
                    if not discord.utils.get(message.server.channels, name=str(All_subjects[i].replace(' ', '-').lower())) in message.server.channels:
                        print(str(All_subjects[i].replace(' ', '-').lower()))
                    overwrite = discord.PermissionOverwrite()
                    everyone_overwrite = discord.PermissionOverwrite()
                    everyone_overwrite.read_messages = False
                    overwrite.read_messages = True
                    overwrite.send_message = True
                    subs_roles = discord.utils.get(message.server.roles, name=All_subjects[i])
                    subs_channels = discord.utils.get(message.server.channels, name=str(All_subjects[i].replace(' ', '-').lower()))
                    try:
                        await client.edit_channel_permissions(subs_channels, subs_roles, overwrite)
                        await client.edit_channel_permissions(subs_channels, discord.utils.get(message.server.roles, name='Members'), everyone_overwrite)
                    except:
                        try:
                            everyone_overwrite = discord.ChannelPermissions(target=discord.utils.get(message.server.roles, name='Members'), overwrite=everyone_overwrite)
                            overwrite = discord.ChannelPermissions(target=subs_roles, overwrite=overwrite)
                            await client.create_channel(message.server , All_subjects[i].replace(' ', '-').lower() , everyone_overwrite, overwrite)
                        except:
                            overwrite = discord.PermissionOverwrite()
                            everyone_overwrite = discord.PermissionOverwrite()
                            everyone_overwrite.read_messages = False
                            overwrite.read_messages = True
                            overwrite.send_message = True
                            everyone_overwrite = discord.ChannelPermissions(target=discord.utils.get(message.server.roles, name='Members'), overwrite=everyone_overwrite)
                            overwrite = discord.ChannelPermissions(target=subs_roles, overwrite=overwrite)
                            await client.create_channel(message.server , All_subjects[i].replace(' ', '-').lower() , everyone_overwrite, overwrite)
                        print('Added Channel '+All_subjects[i].replace(' ', '-').lower())
                        time.sleep(5)
                await client.send_message(message.channel, 'Done')
                return
            elif str(rep.content).lower() == 'user':
                pass
            else:
                return
        if str(message.author.id) in users.keys():
            await client.send_message(message.channel, 'getting things ready')
            typ = 1
        else:
            typ = 0
        if 'Meth Addict' in message.author.roles and not 'Abos' in message.author.roles:
            return
        print('setting up', str(message.author))
        u = str(message.author.id)
        await client.send_message(message.channel, 'What is ur Sex?')
        rep = await client.wait_for_message(author=message.author)
        s = str(rep.content)
        await client.send_message(message.channel, 'Do you have any hobbies')
        rep = await client.wait_for_message(author=message.author)
        h = str(rep.content)
        await client.send_message(message.channel, 'Do you do Maths B? ("yes" or "no")')
        rep = await client.wait_for_message(author=message.author)
        if rep.content.lower() == 'yes':
            ma1='1'
        else:
            ma1='0'
        await client.send_message(message.channel, 'Do you do Maths A or C? ("A" or "C" or "null")')
        rep = await client.wait_for_message(author=message.author)
        if rep.content.lower() == 'a':
            ma2='1'
        elif rep.content.lower() == 'c':
            ma2='2'
        else:
            ma2='0'
        await client.send_message(message.channel, 'What English do you do? ("Adv","Com","null")')
        rep = await client.wait_for_message(author=message.author)
        if rep.content.lower() == 'adv':
            en='2'
        elif rep.content.lower() == 'com':
            en='0'
        else:
            en='1'
        await client.send_message(message.channel, 'Subjects will be split up in 3-4 section depending on what maths subject you have set')
        choices=""
        counter=0
        for i in Electives:
            choices+= i +" = "+Electives[i]+", " if counter != len(Electives)-1 else i +" = "+Electives[i]
            counter+=1
        await client.send_message(message.channel, choices)
        await client.send_message(message.channel, "----")
        while True:
            await client.send_message(message.channel, 'Subject One:')
            rep = await client.wait_for_message(author=message.author)
            if str(rep.content).lower().strip() in Electives:       #----------------------------------------------first one
                e1 = Electives[str(rep.content).lower()]
                break
            elif 'quit' in rep.content.lower():
                return
            else:
                await client.send_message(message.channel, 'not a subject. type quit to stop')
        while True:
            await client.send_message(message.channel, 'Subject Two:')
            rep = await client.wait_for_message(author=message.author)
            if str(rep.content.lower()) in Electives:
                e2 = Electives[str(rep.content).lower()]
                break
            elif 'quit' in rep.content.lower():
                return
            else:
                await client.send_message(message.channel, 'not a subject. type quit to stop')
        while True:
            await client.send_message(message.channel, 'Subject Three:')
            rep = await client.wait_for_message(author=message.author)
            if str(rep.content).lower() in Electives:
                e3 = Electives[str(rep.content).lower()]
                break
            elif 'quit' in rep.content.lower():
                return
            else:
                await client.send_message(message.channel, 'not a subject. type quit to stop')
        if ma1 == '0' or ma2 == '0':
            while True:
                await client.send_message(message.channel, 'Subject Four:')
                rep = await client.wait_for_message(author=message.author)
                if str(rep.content).lower() in Electives:
                    e4 = Electives[str(rep.content).lower()]
                    break
                elif 'quit' in rep.content.lower():
                    return
                else:
                    await client.send_message(message.channel, 'not a subject. type quit to stop')
        else:
            e4='null'
        await client.send_message(message.channel, 'All done to veiw your profile type ```!profile``` like this:')
        await client.send_message(message.channel, '!profile @'+str(message.author))
        await client.add_roles(message.author, member_role)
        await addinguser(typ, u, s, h, e1, e2, e3, e4, ma1, ma2, en)
        await users[message.author.id].roleadd(message)
        async for msg in client.logs_from(message.channel):
            if msg.author.id != '341164679701463040':
                await client.delete_message(msg)
            if msg.content == 'setting up':
                await client.delete_message(msg)
                break
    if message.content.startswith('!bot'):
        await client.send_message(message.channel, 'Super-boi > death')
    if message.content.lower().startswith('!profile'):
        print('getting info...')
        math=''
        try:
            s=message.mentions[0]
            if s.id in users.keys():
                print(s.id)
                name=s
            elif s:
                if 'robotic_abos' in users[s.id].roles(s):
                    await client.send_message(message.channel, 'This Boi is an too good for profiles')
                    return
                print(s.id)
                name=s
                embed = discord.Embed(title="Status "+str(name.status), description="**Info Below**", color=0x00ff00)
                embed.add_field(name="Name:", value=name.name, inline=True)
                embed.add_field(name="ID:", value=name.id, inline=True)
                embed.set_thumbnail(url=name.avatar_url)
               	embed.add_field(name="Sex:", value=users[name.id].sex, inline=True)
                embed.add_field(name="Hobbies:", value=users[name.id].hobbies, inline=True)
                Jointime = datetime.datetime.utcfromtimestamp(datetime.datetime.now().timestamp()-name.joined_at.timestamp()-(10*3600))
                print(datetime.datetime.now().timestamp()-name.joined_at.timestamp()-(10*3600))
                Jointimes=""
                if int(Jointime.strftime("%y"))-70 != 0:
                    Jointimes+= str(int(Jointime.strftime("%y"))-70)+" Years "
                if int(Jointime.strftime("%m"))-1 != 0:
                    Jointimes+= str(int(Jointime.strftime("%m"))-1)+" Months "
                if int(Jointime.strftime("%d"))-1 != 0:
                    Jointimes+=str(int(Jointime.strftime("%d"))-1)+" Days "
                Jointimes+=Jointime.strftime("%H")+" Hours "
                Jointimes+=Jointime.strftime("%M")+" Minutes ago"
                print(datetime.datetime.utcfromtimestamp(int(name.joined_at.timestamp())-36000))
                embed.add_field(name="Joined:", value=Jointimes, inline=False)
                embed.add_field(name="Roles:", value=await users[name.id].roles(name), inline=False)
                embed.set_footer(text="Super-Bot.py did this amazing shit props to creator @hello?#8807")
                await client.send_message(message.channel, embed=embed)
                await client.send_message(message.channel, 'Ask @'+ str(s) +' Setup thier profile before doing this command on him/her ```!setup```')
                return
        except:
            name=message.author
            if not str(message.author.id) in users.keys():
                await client.send_message(message.channel, 'Setup your profile before doing this command ```!setup```')
                return
        subrole=[discord.utils.get(message.server.roles, name=users[name.id].sub1),discord.utils.get(message.server.roles, name=users[name.id].sub2),discord.utils.get(message.server.roles, name=users[name.id].sub3)]
        embed = discord.Embed(title="Status "+str(name.status), description="**Info Below**", color=0x00ff00)
        embed.add_field(name="Name:", value=name.name, inline=True)
        embed.add_field(name="ID:", value=name.id, inline=True)
        embed.set_thumbnail(url=name.avatar_url)
       	embed.add_field(name="Sex:", value=users[name.id].sex, inline=True)
        embed.add_field(name="Hobbies:", value=users[name.id].hobbies, inline=True)
        Jointime = datetime.datetime.utcfromtimestamp(datetime.datetime.now().timestamp()-name.joined_at.timestamp()-(10*3600))
        print(datetime.datetime.now().timestamp()-name.joined_at.timestamp()-(10*3600))
        Jointimes=""
        if int(Jointime.strftime("%y"))-70 != 0:
            Jointimes+= str(int(Jointime.strftime("%y"))-70)+" Years "
        if int(Jointime.strftime("%m"))-1 != 0:
            Jointimes+= str(int(Jointime.strftime("%m"))-1)+" Months "
        if int(Jointime.strftime("%d"))-1 != 0:
            Jointimes+=str(int(Jointime.strftime("%d"))-1)+" Days "
        Jointimes+=Jointime.strftime("%H")+" Hours "
        Jointimes+=Jointime.strftime("%M")+" Minutes ago"
        print(datetime.datetime.utcfromtimestamp(int(name.joined_at.timestamp())-36000))
        #Jointime.strftime("%-d Days %-m Months ")+str(int((Jointime.strftime("%y")))-70)+Jointime.strftime("%-h Hours %-m Minutes")
        #Jointime.strptime("%d")+" Day "+Jointime.strptime("%m")+" Month "+str(int(Jointime.strftime("%y"))-70)+" Year "
        embed.add_field(name="Joined:", value=Jointimes, inline=False)
        embed.add_field(name="Roles:", value=await users[name.id].roles(name), inline=False)
        embed.add_field(name="-----------------", value='**Subjects**', inline=False)
        embed.add_field(name="Maths:", value=users[name.id].readable('M'), inline=True)
        embed.add_field(name="English", value=users[name.id].readable('E'), inline=True)
        embed.add_field(name="Subject 1:", value=users[name.id].sub1, inline=False)
        embed.add_field(name="Subject 2:", value=users[name.id].sub2, inline=False)
        embed.add_field(name="Subject 3:", value=users[name.id].sub3, inline=False)
        if users[name.id].sub4 != 'null':
            embed.add_field(name="Subject 4:", value=users[name.id].sub4, inline=False)
            subrole.append(discord.utils.get(message.server.roles, name=users[name.id].sub4))
        embed.set_footer(text="Super-Boi.py did this amazing shit props to creator @hello?#8807")
        await client.send_message(message.channel, embed=embed)
        print('done')
        await users[message.author.id].roleadd(message)
        if name.id == '341164679701463040':
            await client.add_roles(name, discord.utils.get(message.server.roles, name='Abos'))
    if message.content.lower() == 'ur mom gay':
        await client.send_message(message.channel, 'no you')
    if message.content.lower().startswith('!marry'):
        embed = discord.Embed(title="Do you want to have a never ending headache :ring:", description="Do you want to marry"+str(message.mentions[0]), color=0x00ff00)
        embed.set_footer(text="Reply with yes or no")
        await client.send_message(message.channel, embed=embed)
        rep = await client.wait_for_message(author=message.mentions[0])
        if rep.content.lower() == 'yes':
            await client.send_message(message.channel, "ooo wee, a Marriage")
            married[str(message.author.id)]=str(message.mentions[0].id)
            married[str(message.mentions[0].id)]=str(message.author.id)
        else:
            await client.send_message(message.channel, "commit suicide now, no one loves you")
    if message.content.startswith(';marry') and message.mentions[0].id == client.user.id:
        await client.wait_for_message(author=discord.utils.get(message.server.members, name='death'))
        if message.author.id != '410392481553973258':
            await client.send_message(message.channel, "yes")
        else:
            await client.send_message(message.channel, "no bitch!")
    if message.content.lower().startswith("!list"):
        await client.send_message(message.channel, "list of Users:")
        for i in users:
            await client.send_message(message.channel, str(await users[i].idtouser(message).name))
    if message.content.lower().startswith('!role'):
        if message.author.id in users.keys():
            await users[message.author.id].roleadd(message)
            await client.add_roles(message.author, discord.utils.get(message.server.roles, name='Members'))
        else:
            await client.send_message(message.channel, "Please do setup to have acess to this command")
    if message.content.lower().startswith('!showall'):
        role = discord.utils.get(message.server.roles, name='Show All')
        if role in message.author.roles:
            await client.remove_roles(message.author, discord.utils.get(message.server.roles, name='Show All'))
        else:
            await client.add_roles(message.author, discord.utils.get(message.server.roles, name='Show All'))
client.run('NDA3NjY1NzEzNDk5NjAyOTQ3.DX0kyQ.m8lZMlUfy39PajRtyimysmZOn4s')
